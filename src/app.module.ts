import { Module } from '@nestjs/common';

import { SmartphonesModule } from './modules/smartphones/smartphones.module';
import { AuditsModule } from './modules/audits/audits.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    SmartphonesModule,
    AuditsModule,
    DatabaseModule,
  ],
})
export class AppModule {
  static port: number;
  constructor(private readonly configService: ConfigService) {
    AppModule.port = +this.configService.get('PORT');
  }
}
