import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true, // validation DTO
      whitelist: true, // return when body is with unnecessary properties
      forbidNonWhitelisted: true, // return error when body is with unnecessary properties
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  app.enableCors(); // for develop at frontend
  await app.listen(AppModule.port); // AppModule.port = 3100
}
bootstrap();
