import { Smartphone } from 'src/modules/smartphones/smartphone.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Audit {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: false })
  name: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne((type) => Smartphone, (smartphone) => smartphone.audits)
  @JoinColumn({ name: 'smartphone_id' })
  smartphone: Smartphone;
}
