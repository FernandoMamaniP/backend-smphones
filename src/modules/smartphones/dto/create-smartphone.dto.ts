import { IsArray, IsString } from 'class-validator';
import { Audit } from 'src/modules/audits/entities/audit.entity';

export class CreateSmartphoneDto {
  @IsString()
  readonly name: string;
  @IsString()
  readonly model: string;
  @IsString()
  readonly priceTag: string;
  @IsString()
  readonly salePrice: string;
  @IsString()
  readonly modelYear: string;
  @IsArray()
  readonly audits: Partial<Audit>[];
}
