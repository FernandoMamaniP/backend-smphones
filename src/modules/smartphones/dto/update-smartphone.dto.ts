import { IsString } from 'class-validator';

export class UpdateSmartphoneDto {
  @IsString()
  readonly model: string;
}
