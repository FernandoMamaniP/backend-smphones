import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Audit } from '../audits/entities/audit.entity';

@Entity()
export class Smartphone {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name?: string;

  @Column()
  model: string;

  @Column()
  priceTag?: string;

  @Column()
  salePrice?: string;

  @Column()
  modelYear?: string;

  @OneToMany((type) => Audit, (audit) => audit.smartphone, {
    cascade: true,
  })
  audits: Audit[];
}
