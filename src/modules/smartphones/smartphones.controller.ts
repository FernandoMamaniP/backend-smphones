import { Controller, Get } from '@nestjs/common';
import {
  Body,
  Delete,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common/decorators/http';
import { SmartphonesService } from './smartphones.service';
import { Smartphone } from './smartphone.entity';
import { CreateSmartphoneDto, PaginationQueryDto } from './dto';

@Controller('smartphones')
export class SmartphonesController {
  constructor(private readonly smartphonesService: SmartphonesService) {}

  @Get()
  getSmartphones(
    @Query() pagination: PaginationQueryDto,
  ): Promise<Smartphone[] | number> {
    if (pagination.count) {
      return this.smartphonesService.getCountSmartphones();
    } else {
      return this.smartphonesService.getSmartphones(pagination);
    }
  }

  @Get(':id')
  getSmartphone(@Param('id') id: number): Promise<Smartphone> {
    return this.smartphonesService.getSmartphone(id);
  }

  @Post()
  createSmartphone(
    @Body() smartPhone: CreateSmartphoneDto,
  ): Promise<Smartphone> {
    return this.smartphonesService.createSmartphone(smartPhone);
  }

  @Patch(':id')
  updateSmartphone(
    @Param('id') id: number,
    @Body() smartphone: object,
  ): Promise<Smartphone> {
    return this.smartphonesService.updateSmartphone(id, smartphone);
  }

  @Delete(':id')
  deleteSmartphone(@Param('id') id: number): Promise<void> {
    return this.smartphonesService.deleteSmartphone(id);
  }
}
