import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SmartphonesService } from './smartphones.service';
import { SmartphonesController } from './smartphones.controller';
import { Smartphone } from './smartphone.entity';
import { Audit } from '../audits/entities/audit.entity';
@Module({
  imports: [TypeOrmModule.forFeature([Smartphone, Audit])],
  controllers: [SmartphonesController],
  providers: [SmartphonesService],
})
export class SmartphonesModule {}
