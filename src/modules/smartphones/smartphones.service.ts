import { Injectable, NotFoundException } from '@nestjs/common';
import { Smartphone } from './smartphone.entity';
import { CreateSmartphoneDto, PaginationQueryDto } from './dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Audit } from '../audits/entities/audit.entity';

@Injectable()
export class SmartphonesService {
  constructor(
    @InjectRepository(Smartphone)
    private readonly smartphoneRepository: Repository<Smartphone>,
    @InjectRepository(Audit)
    private readonly auditRepository: Repository<Audit>,
  ) {}

  async getSmartphones({
    limit,
    offset,
  }: PaginationQueryDto): Promise<Smartphone[]> {
    return await this.smartphoneRepository.find({
      relations: ['audits'],
      skip: offset,
      take: limit,
    });
  }

  async getSmartphone(id: number): Promise<Smartphone> {
    const findSmartphone: Smartphone = await this.smartphoneRepository.findOne({
      where: { id },
      relations: ['audits'],
    });
    if (!Boolean(findSmartphone)) {
      throw new NotFoundException('Resource not found');
    }
    return findSmartphone;
  }

  async createSmartphone(smartPhone: CreateSmartphoneDto): Promise<Smartphone> {
    const newSmartphone: Smartphone =
      this.smartphoneRepository.create(smartPhone);
    return this.smartphoneRepository.save(newSmartphone);
  }

  async updateSmartphone(id: number, smartphone: object): Promise<Smartphone> {
    const findSmartphone: Smartphone = await this.smartphoneRepository.findOne({
      where: { id },
    });
    if (!Boolean(findSmartphone)) {
      throw new NotFoundException('Resource not found');
    }
    await this.smartphoneRepository.update(id, smartphone);
    const newSmartphone: Smartphone = await this.smartphoneRepository.findOne({
      where: { id },
    });
    return newSmartphone;
  }

  async deleteSmartphone(id: number): Promise<void> {
    const findSmartphone: Smartphone = await this.smartphoneRepository.findOne({
      where: { id },
    });
    if (!Boolean(findSmartphone)) {
      throw new NotFoundException('Resource not found');
    }
    await this.auditRepository.delete({
      smartphone: findSmartphone,
    });
    await this.smartphoneRepository.remove(findSmartphone);
  }

  async getCountSmartphones(): Promise<number> {
    return await this.smartphoneRepository.count();
  }
}
